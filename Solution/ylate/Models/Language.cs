﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ylate.Models
{
    public class Language
    {
        public int Id { get; set; }
        public Guid ProjectLanguageId { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string NativeName { get; set; }
        public string IconUrl { get; set; }
        public int TotalKeywordCount { get; set; }
        public int TotalKeywordTranslatedCount { get; set; }
    }
    public class AllLanguageResponse : ResponseBase
    {
        public List<Language> Language { get; set; }
    }
    public class AllLanguageList : ResponseBase
    {
        public Language AllLanguage { get; set; }
    }
}

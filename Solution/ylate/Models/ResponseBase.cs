﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ylate.Models
{
    public class ResponseBase
    {
        public string Error { get; set; }
        public int ErrCode { get; set; }
        public static T Ok<T>() where T: ResponseBase, new()
        {
            return new T();
        }
        public static T Fail<T>(string err) where T: ResponseBase, new()
        {
            return new T() { Error = err };
        }
    }
}

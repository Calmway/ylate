﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ylate.EF;

namespace ylate.Models
{
    public class ProjectModel
    {
    }
    public class GetProjectNameRequest
    {
        public string ProjectKey { get; set; }
    }
    public class GetProjectNameResponse : ResponseBase
    {
        public string ProjectName { get; set; }
        public Guid ProjectId { get; set; }
    }
    public class CreateProjectRequest
    {
        public string NewName { get; set; }
        public string ProjectKey { get; set; }
    }
    public class CreateProjectResponse : ResponseBase
    {
        public Guid Id { get; set; }
        public string ProjectName { get; set; }
        public string ProjectKey { get; set; }

    }

    public class IdTextModel
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ProjectKeywords { get; set; }
        public int ProjectLanguages { get; set; }
        public int ProjectTranslatedKeywords { get; set; }
        public string ProjectKey { get; set; }
    }
    public class GetProjectList : ResponseBase
    {
        public List<IdTextModel> ProjectList { get; set; }
    }

    public class UpdateProjectRequest
    {
        public string Id { get; set; }
        public string NewName { get; set; }
    }
    public class UpdateProjectResponse : ResponseBase
    {

    }

    public class DeleteProjectRequest
    {
        public string Id { get; set; }
    }
    public class DeleteProjectResponse : ResponseBase
    {
        public Guid Id { get; set; }
    }
}

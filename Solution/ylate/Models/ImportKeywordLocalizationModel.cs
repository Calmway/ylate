﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ylate.Models
{
    public class ImportKeywordLocalizationModel
    {
        public IFormCollection json { get; set; }
        public string ProjectId { get; set; }
        public string LangId { get; set; }
    }

    public class ImportExcel
    {
        public IFormCollection excel { get; set; }
        public string ProjectId { get; set; }
        public string LangId { get; set; }
    }
    public class ProjectLanguage
    {
        public Guid Id { get; set; }
        public string EnglishName { get; set; }
    }
}

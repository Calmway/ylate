﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ylate.EF;

namespace ylate.Models
{
    public class ProjectLocalizationModel
    {
    }

    public class AddProjectLocalizationRequest
    {
        public string ProjectId { get; set; }
        public string KeywordId { get; set; }
        public string LocalizationValue { get; set; }
        public string LanguageId { get; set; }
    }
    public class AddProjectLocalizationResponse : ResponseBase
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public Guid KeywordId { get; set; }
        public string LocalizationValue { get; set; }
        public Guid LanguageId { get; set; }
    }
    public class GetProjectLocalizationValueRequest
    {
        public string ProjectId { get; set; }
        public string KeywordId { get; set; }
        public string LanguageId { get; set; }
    }
    public class GetProjectLocalizationValueResponse : ResponseBase
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public Guid KeywordId { get; set; }
        public string LocalizationValue { get; set; }
        public Guid LanguageId { get; set; }
    }

    public class UpdateProjectLocalizationRequest
    {
        public string Value { get; set; }
        public string Id { get; set; }
    }
    public class UpdateProjectLocalizationResponse : ResponseBase
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public Guid KeywordId { get; set; }
        public string LocalizationValue { get; set; }
        public Guid LanguageId { get; set; }
    }
    public class DeleteProjectLocalizationRequest
    {
        public string Id { get; set; }
    }
    public class DeletProjectLocalizationResponse : ResponseBase
    {
        public Guid? Id { get; set; }
        public Guid? KeywordId { get; set; }
    }
}

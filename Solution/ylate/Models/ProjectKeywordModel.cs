﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ylate.EF;

namespace ylate.Models
{
    public class ProjectKeywordModel
    {
    }
    public class CreateKeywordRequest
    {
        public string NewKeyword { get; set; }
        public string ProjectId { get; set; }
    }
    public class CreateKeywordResponse : ResponseBase
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public string Keyword { get; set; }
    }
    public class GetProjectKeywordListRequest
    {
        //public string ProjectId { get; set; }
        public string ProjectKey { get; set; }
        //public string LangId { get; set; }
        public int LCID { get; set; }
    }
    public class GetProjectKeywordRequest
    {
        public string ProjectKeywordId { get; set; }
        public string ProjectKey { get; set; }
        public int LCID { get; set; }
    }
    public class GetProjectKeywordsRequest
    {
        public string ProjectKey { get; set; }
        public string ProjectKeyword { get; set; }
    }
    public class IdKeyword
    {
        public Guid Id { get; set; }
        public string Keyword { get; set; }
        //public bool HasLocalization { get; set; }
        public string Value { get; set; }
    }
    public class IdLocalizations
    {
        public int LCID { get; set; }
        public string EnglishName { get; set; }
        public List<Guid> IdKeywords { get; set; }
    }
    public class IdKeywords
    {
        public Guid Id { get; set; }
        public string Keyword { get; set; }
    }
    public class GetProjectKeywordList : ResponseBase
    {
        public List<IdKeyword> Keywords { get; set; }
        public Guid ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string LanguageName { get; set; }
        public Guid ProjectLangId { get; set; }
        public string ProjectKey { get; set; }
    }
    public class GetProjectKeywords : ResponseBase
    {
        public List<IdKeywords> Keywords { get; set; }
        public List<IdLocalizations> Localizations { get; set; }
        public Guid ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectNameKey { get; set; }
    }
    public class GetProjectLanguageIdRequest
    {
        public string ProjectId { get; set; }
        public int LCID { get; set; }
    }
    public class GetProjectLanguageIdResponse: ResponseBase
    {
        public Guid LanguageId { get; set; }
    }
    public class UpdateKeywordRequest
    {
        public string Id { get; set; }
        public string NewKeyword { get; set; }
    }
    public class UpdateKeywrodResponse : ResponseBase
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public string Keyword { get; set; }
    }
    public class DeleteKeywordRequest
    {
        public string Id { get; set; }
    }
    public class DeletKeywordResponse : ResponseBase
    {
        public Guid? Id { get; set; }
    }
    public class DeleteKeywordsRequest
    {
        public string ProjectId { get; set; }
    }
    public class DeleteKeywordsResponse : ResponseBase
    {
        public Guid? Id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ylate.Models
{
    public class DownloadRequestModel : ResponseBase
    {
        public string ProjectId { get; set; }
        public string LanguageId { get; set; }
        public string Format { get; set; }
        //public string SplitValue { get; set; }
    }
    public class ExportRequestModel : ResponseBase
    {
        public string ProjectKey { get; set; }
        public int LCID { get; set; }
        public string Format { get; set; }
        //public string SplitValue { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ylate.Models
{
    public class ImportKeywordsModel
    {
        public IFormCollection json { get; set; }
        public string ProjectId { get; set; }
    }
}

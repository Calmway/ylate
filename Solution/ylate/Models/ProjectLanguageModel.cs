﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ylate.EF;

namespace ylate.Models
{
    public class ProjectLanguageModel
    {

    }
    public class AddProjectLanguageRequest
    {
        public List<int> LCID { get; set; }
        public string ProjectId { get; set; }
    }
    public class AddProjectLanguageResponse : ResponseBase
    {
        public List<Language> Langs { get; set; }
    }
    public class GetProjectLanguageListRequest
    {
        //public string ProjectId { get; set; }
        public string ProjectKey { get; set; }
    }
    public class GetProjectLanguageListResponse : ResponseBase
    {
        //public Guid Id { get; set; }
        //public Guid ProjectId { get; set; }
        //public string LCID { get; set; }

        public List<Language> Languages { get; set; }
    }

    public class UpdateProjectLanguageRequest
    {
        public int LCID { get; set; }
        public string Id { get; set; }
    }
    public class UpdateProjectLanguageResponse : ResponseBase
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public int LCID { get; set; }
    }
    public class DeleteProjectLanguageRequest
    {
        public string Id { get; set; }
        public string ProjectId { get; set; }
        public int LCID { get; set; }
    }
    public class DeleteProjectLanguageResponse : ResponseBase
    {
        // public ProjectLanguage ProjectKeyword { get; set; }
        public Guid? Id { get; set; }
    }
    public class GetProjectLanguageNameRequest
    {
        public int LCID { get; set; }
    }
    public class GetProjectLanguageNameRepsonse : ResponseBase
    {
        public string EnglishName { get; set; }
    }
}

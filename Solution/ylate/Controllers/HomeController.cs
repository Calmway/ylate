﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using ylate.EF;
using ylate.Models;

namespace ylate.Controllers
{
    [Route("api/project")]
    public class HomeController : YlateMvcControllerBase
    {
        public HomeController(ILifetimeScope lifetimeScope) : base(lifetimeScope) { }

        public IActionResult Index()
        {

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        /********************************************************/
        /***************PROJECT REQUEST & RESPONSE***************/
        /********************************************************/

        [HttpPost("create")]
        public CreateProjectResponse AddNewProject([FromBody]CreateProjectRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<CreateProjectResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.NewName))
            {
                return ResponseBase.Fail<CreateProjectResponse>("New Project name is Empty");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectKey))
            {
                return ResponseBase.Fail<CreateProjectResponse>("New ProjectKey is Empty");
            }
            if (request.ProjectKey.Length > 10 && Regex.IsMatch(request.ProjectKey, @"^[a-zA-Z0-9]+$"))
            {
                return ResponseBase.Fail<CreateProjectResponse>("Incorrect ProjectKey");
            }

            var db = Resolve<DbContext>();
            if (db.Projects.Any(x=>x.ProjectKey == request.ProjectKey))
            {
                return ResponseBase.Fail<CreateProjectResponse>("New ProjectKey already exist");
            }
            var resp = new CreateProjectResponse();
            var np = new ProjectEntity()
            {
                ProjectName = request.NewName,
                ProjectKey = request.ProjectKey
            };
            db.Projects.Add(np);
            db.SaveChanges();
            resp.Id = np.Id;
            resp.ProjectName = np.ProjectName;
            resp.ProjectKey = np.ProjectKey;
            return resp;
        }
        [HttpPost("getprojid")]
        public GetProjectNameResponse GetProjectName([FromBody]GetProjectNameRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<GetProjectNameResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectKey))
            {
                return ResponseBase.Fail<GetProjectNameResponse>("Invalid ProjectId");
            }
            //if (!Guid.TryParse(request.Id, out Guid objGuid))
            //{
            //    return ResponseBase.Fail<GetProjectNameResponse>("Invalid ProjectId");
            //}

            var db = Resolve<DbContext>();
            var project = db.Projects.FirstOrDefault(x => x.ProjectKey == request.ProjectKey);
            if (project == null)
            {
                return ResponseBase.Fail<GetProjectNameResponse>("Project not found");
            }
            GetProjectNameResponse projectresp = new GetProjectNameResponse()
            {
                ProjectName = project.ProjectName,
                ProjectId = project.Id
            };
            return projectresp;
        }



        [HttpGet("list")]
        public GetProjectList GetListOfProjects()
        {
            var db = Resolve<DbContext>();
            var p = db.Projects.ToList().OrderBy(x=>x.ProjectName);
            List<Guid> ProjArra = new List<Guid>();
            GetProjectList projectResp = new GetProjectList()
            {
                ProjectList = p.Select(x => new IdTextModel
                {
                    Id = x.Id,
                    Text = x.ProjectName,
                    CreatedDate = x.CreatedAt,
                    ProjectKey = x.ProjectKey
                }).ToList()
            };

            foreach (var item in projectResp.ProjectList)
            {
                var projkeys = db.ProjectKeywords.Where(x => x.ProjectId == item.Id).Count();
                var projlangs = db.ProjectLanguages.Where(x => x.ProjectId == item.Id).Count();
                var projtranslatedkeys = db.ProjectLocalizations.Where(x => x.ProjectId == item.Id && x.Value != null).Count();
                item.ProjectKeywords = projkeys;
                item.ProjectTranslatedKeywords = projtranslatedkeys;
                item.ProjectLanguages = projlangs;
            }


            return projectResp;
        }

        [HttpPost("update")]
        public UpdateProjectResponse UpdateNameOfProject([FromBody]UpdateProjectRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<UpdateProjectResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.Id))
            {
                return ResponseBase.Fail<UpdateProjectResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(request.Id, out Guid objGuid))
            {
                return ResponseBase.Fail<UpdateProjectResponse>("Invalid ProjectId");
            }
            if (String.IsNullOrWhiteSpace(request.NewName))
            {
                return ResponseBase.Fail<UpdateProjectResponse>("New Project name is Empty");
            }

            var db = Resolve<DbContext>();
            var project = db.Projects.FirstOrDefault(x => x.Id == objGuid);
            if (project == null)
            {
                return ResponseBase.Fail<UpdateProjectResponse>("Project not found");
            }
            project.ProjectName = request.NewName;
            db.SaveChanges();
            return ResponseBase.Ok<UpdateProjectResponse>();
        }

        [HttpPost("delete")]
        public DeleteProjectResponse DeleteProject([FromBody]DeleteProjectRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<DeleteProjectResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.Id))
            {
                return ResponseBase.Fail<DeleteProjectResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(request.Id, out Guid objGuid))
            {
                return ResponseBase.Fail<DeleteProjectResponse>("Invalid ProjectId");
            }

            var db = Resolve<DbContext>();
            var resp = new DeleteProjectResponse();
            var np = new ProjectEntity()
            {
                Id = objGuid
            };
            db.Projects.Attach(np);
            db.Projects.Remove(np);
            db.SaveChanges();
            resp.Id = np.Id;
            return resp;
        }

        /**********************************************************/
        /***************KEYWORDS REQUEST AND RESPONSE**************/
        /**********************************************************/
        [HttpPost("addkeyword")]
        public CreateKeywordResponse AddNewKeyword([FromBody]CreateKeywordRequest request)
        {
            //Regex regex = new Regex("(:{4,})|([^:]:{1,2}[^:])|(:$)|(^:)|([!@#$%^&*)(_\"\'}{[;?/,.|-])");
            if (request == null)
            {
                return ResponseBase.Fail<CreateKeywordResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectId))
            {
                return ResponseBase.Fail<CreateKeywordResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(request.ProjectId, out Guid objGuid))
            {
                return ResponseBase.Fail<CreateKeywordResponse>("Invalid ProjectId");
            }
            if (String.IsNullOrWhiteSpace(request.NewKeyword))
            {
                return ResponseBase.Fail<CreateKeywordResponse>("New keyword is empty");
            }

            //var validated = regex.Matches(request.NewKeyword);
            
            //if (validated.Count>=1)
            //{
            //    return ResponseBase.Fail<CreateKeywordResponse>("New keyword has incorrect format");
            //}

            var db = Resolve<DbContext>();
            var resp = new CreateKeywordResponse();
            var Existing = db.ProjectKeywords.Any(x => x.ProjectId == objGuid && (x.Keyword == request.NewKeyword || x.Keyword.StartsWith(request.NewKeyword + ":::") || request.NewKeyword.StartsWith(x.Keyword + ":::")));
            if (Existing)
            {
                
                return ResponseBase.Fail<CreateKeywordResponse>("New keyword is already exist or incorrect level format.");
            }

            var np = new ProjectKeywordEntity()
            {
                Keyword = request.NewKeyword,
                ProjectId = objGuid
            };
            db.ProjectKeywords.Add(np);
            db.SaveChanges();
            resp.Id = np.Id;
            resp.ProjectId = np.ProjectId;
            resp.Keyword = np.Keyword;

            return resp;
        }

        [HttpGet("listkeyword")]
        public List<String> GetListOfProjectKeywrods()
        {
            var db = Resolve<DbContext>();
            var p = db.ProjectKeywords.Select(x => x.Keyword).ToList();
            return p;
        }
        [HttpPost("projectlanguageId")]
        public GetProjectLanguageIdResponse GetProjectLanguageIdRequest([FromBody] GetProjectLanguageIdRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<GetProjectLanguageIdResponse>("Request Error");
            }
            if (request.LCID <= 0)
            {
                return ResponseBase.Fail<GetProjectLanguageIdResponse>("Invalid LCID");
            }
            if (!Guid.TryParse(request.ProjectId, out Guid objGuid))
            {
                return ResponseBase.Fail<GetProjectLanguageIdResponse>("Invalid ProjectId");
            }
            var db = Resolve<DbContext>();
            var p = db.ProjectLanguages.FirstOrDefault(x => x.ProjectId == objGuid && x.LCID == request.LCID);
            var resp = new GetProjectLanguageIdResponse();
            resp.LanguageId = p.Id;
            return resp;
        }
        [HttpPost("projectlistkeyword")]
        public GetProjectKeywordList GetfProjectListKeywrods([FromBody]GetProjectKeywordListRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<GetProjectKeywordList>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectKey))
            {
                return ResponseBase.Fail<GetProjectKeywordList>("Invalid ProjectId");
            }
            if (request.LCID <=0)
            {
                return ResponseBase.Fail<GetProjectKeywordList>("Invalid LCID");
            }
            var db = Resolve<DbContext>();
            var project = db.Projects.FirstOrDefault(x => x.ProjectKey == request.ProjectKey);
            if (project==null)
            {
                return ResponseBase.Fail<GetProjectKeywordList>("Project not found");
            }
            var projectlanguage = db.ProjectLanguages.FirstOrDefault(x => x.ProjectId == project.Id && x.LCID == request.LCID);
            if (projectlanguage == null)
            {
                return ResponseBase.Fail<GetProjectKeywordList>("Language not found");
            }
            //case when[Value] is not null then CAST(1 AS BIT) else CAST(0 AS BIT) end as HasLocalization,
            var sql = $@"SELECT ProjectKeywords.Id, Keyword, ProjectLocalization.Value
            FROM
	            ProjectKeywords
	            LEFT JOIN ProjectLocalization
							ON ProjectLocalization.KeywordId=ProjectKeywords.Id AND ProjectLocalization.LanguageId=@langid
            WHERE
	            ProjectKeywords.ProjectId = @projid;";
            System.Data.SqlClient.SqlParameter projid = new System.Data.SqlClient.SqlParameter("@projid", project.Id);
            System.Data.SqlClient.SqlParameter langid = new System.Data.SqlClient.SqlParameter("@langid", projectlanguage.Id);
            var res = db.Database.SqlQuery<IdKeyword>(sql, projid, langid).ToList();
            GetProjectKeywordList resp = new GetProjectKeywordList();
            resp.Keywords = res;
            resp.ProjectId = project.Id;
            resp.ProjectName = project.ProjectName;
            resp.LanguageName = projectlanguage.Language.EnglishName;
            resp.ProjectLangId = projectlanguage.Id;
            resp.ProjectKey = project.ProjectKey;
            return resp;
        }
        [HttpPost("projectkeywords")]
        public GetProjectKeywords GetfProjectKeywrods([FromBody]GetProjectKeywordsRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<GetProjectKeywords>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectKey))
            {
                return ResponseBase.Fail<GetProjectKeywords>("Invalid ProjectId");
            }
            var db = Resolve<DbContext>();
            var project = db.Projects.FirstOrDefault(x => x.ProjectKey == request.ProjectKey);
            if (project == null)
            {
                return ResponseBase.Fail<GetProjectKeywords>("Project not found");
            }

            var projectlanguage = db.ProjectLanguages.FirstOrDefault(x => x.ProjectId == project.Id);
            var allkeywords = db.ProjectKeywords.Where(x => x.ProjectId == project.Id).Take(50).ToList();
            var languages = db.ProjectLanguages.Where(x => x.ProjectId == project.Id).ToList();
            var existingLocalizations = db.ProjectLocalizations.Where(x => x.ProjectId == project.Id)
                .GroupBy(x=>x.Language)
                .Select(x=> new IdLocalizations {
                    LCID = x.Key.LCID,
                    EnglishName = x.Key.Language.EnglishName,
                    IdKeywords = x.Select(y=>y.KeywordId).ToList()
                }).ToList();
            var KeywordList = allkeywords.Select(x => new IdKeywords { Id = x.Id, Keyword = x.Keyword }).ToList();
            GetProjectKeywords resp = new GetProjectKeywords();
            resp.Keywords = KeywordList;
            resp.Localizations = existingLocalizations;
            resp.ProjectId = project.Id;
            resp.ProjectName = project.ProjectName;
            resp.ProjectNameKey = project.ProjectKey;
            return resp;
        }


        [HttpPost("searchprojectkeywords")]
        public GetProjectKeywords SearchProjectKeywrods([FromBody] GetProjectKeywordsRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<GetProjectKeywords>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectKey))
            {
                return ResponseBase.Fail<GetProjectKeywords>("Invalid ProjectId");
            }
            var db = Resolve<DbContext>();
            var project = db.Projects.FirstOrDefault(x => x.ProjectKey == request.ProjectKey);
            if (project == null)
            {
                return ResponseBase.Fail<GetProjectKeywords>("Project not found");
            }

            var projectlanguage = db.ProjectLanguages.FirstOrDefault(x => x.ProjectId == project.Id);
            var allkeywords = db.ProjectKeywords.Where(x => x.ProjectId == project.Id && x.Keyword.Contains(request.ProjectKeyword));
            var languages = db.ProjectLanguages.Where(x => x.ProjectId == project.Id).ToList();
            var existingLocalizations = db.ProjectLocalizations.Where(x => x.ProjectId == project.Id)
                .GroupBy(x => x.Language)
                .Select(x => new IdLocalizations
                {
                    LCID = x.Key.LCID,
                    EnglishName = x.Key.Language.EnglishName,
                    IdKeywords = x.Select(y => y.KeywordId).ToList()
                }).ToList();
            var KeywordList = allkeywords.Select(x => new IdKeywords { Id = x.Id, Keyword = x.Keyword }).ToList();
            GetProjectKeywords resp = new GetProjectKeywords();
            resp.Keywords = KeywordList;
            resp.Localizations = existingLocalizations;
            resp.ProjectId = project.Id;
            resp.ProjectName = project.ProjectName;
            resp.ProjectNameKey = project.ProjectKey;
            return resp;
        }

        [HttpPost("updatekeyword")]
        public UpdateKeywrodResponse UpdateKeyword([FromBody]UpdateKeywordRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<UpdateKeywrodResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.Id))
            {
                return ResponseBase.Fail<UpdateKeywrodResponse>("Keyword doesn't exist");
            }
            if (!Guid.TryParse(request.Id, out Guid objGuid))
            {
                return ResponseBase.Fail<UpdateKeywrodResponse>("Invalid Id");
            }
            if (String.IsNullOrWhiteSpace(request.NewKeyword))
            {
                return ResponseBase.Fail<UpdateKeywrodResponse>("New keyword is empty");
            }
            var db = Resolve<DbContext>();
            var Existing = db.ProjectKeywords.Any(x => x.ProjectId == objGuid && (x.Keyword == request.NewKeyword || x.Keyword.StartsWith(request.NewKeyword + ":::") || request.NewKeyword.StartsWith(x.Keyword + ":::")));
            if (Existing)
            {

                return ResponseBase.Fail<UpdateKeywrodResponse>("New keyword is already exist or incorrect level format.");
            }
            var keyword = db.ProjectKeywords.Where(x => x.Id == objGuid).FirstOrDefault();
            keyword.Keyword = request.NewKeyword;
            db.SaveChanges();
            var resp = new UpdateKeywrodResponse();
            resp.Id = keyword.Id;
            resp.Keyword = keyword.Keyword;
            resp.ProjectId = keyword.ProjectId;
            return resp;
        }

        [HttpPost("deletekeyword")]
        public DeletKeywordResponse DeleteKeyword([FromBody]DeleteKeywordRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<DeletKeywordResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.Id))
            {
                return ResponseBase.Fail<DeletKeywordResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(request.Id, out Guid objGuid))
            {
                return ResponseBase.Fail<DeletKeywordResponse>("Invalid ProjectId");
            }
            var db = Resolve<DbContext>();
            var resp = new DeletKeywordResponse();
            var np = new ProjectKeywordEntity()
            {
                Id = objGuid
            };
            db.ProjectLocalizations.RemoveRange(db.ProjectLocalizations.Where(x => x.KeywordId == objGuid));
            db.ProjectKeywords.Attach(np);
            db.ProjectKeywords.Remove(np);
            db.SaveChanges();
            resp.Id = objGuid;
            return resp;

        }

        [HttpPost("deletekeywords")]
        public DeleteKeywordsResponse DeleteKeywordx([FromBody]DeleteKeywordsRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<DeleteKeywordsResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectId))
            {
                return ResponseBase.Fail<DeleteKeywordsResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(request.ProjectId, out Guid objGuid))
            {
                return ResponseBase.Fail<DeleteKeywordsResponse>("Invalid ProjectId");
            }
            var db = Resolve<DbContext>();
            var resp = new DeleteKeywordsResponse();
            var np = new ProjectKeywordEntity()
            {
                ProjectId = objGuid
            };
            db.ProjectKeywords.RemoveRange(db.ProjectKeywords.Where(x => x.ProjectId == objGuid));
            //db.ProjectKeywords.Attach(np);
            //db.ProjectKeywords.Remove(np);
            db.SaveChanges();
            resp.Id = objGuid;
            return resp;

        }
        /********************************************************/
        /*********************GET ALL LANGUAGES******************/
        /********************************************************/

        [HttpGet("listprojectlanguage")]
        public AllLanguageResponse ProjectLanguageList()
        {
            var db = Resolve<DbContext>();
            var p = db.Languages.ToList();
            var languages = p.Select(x => new Language
            {
                Id = x.LCID,
                Name = x.Name,
                EnglishName = x.EnglishName,
                NativeName = x.NativeName,
                IconUrl = x.IconUrl
            }).ToList();
            return new AllLanguageResponse { Language = languages };
        }
        /***********************************************************/
        /***********PROJECTLANGUAGE REQUEST AND RESPONSE***********/
        /*********************************************************/
        [HttpPost("projectlangname")]
        public GetProjectLanguageNameRepsonse GetProjectLangName([FromBody] GetProjectLanguageNameRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<GetProjectLanguageNameRepsonse>("Request Error");
            }
            if (request.LCID <= 0)
            {
                return ResponseBase.Fail<GetProjectLanguageNameRepsonse>("Invalid LCID");
            }

            var db = Resolve<DbContext>();
            var getEngName = db.Languages.FirstOrDefault(x => x.LCID == request.LCID);
            var resp = new GetProjectLanguageNameRepsonse()
            {
                EnglishName = getEngName.EnglishName
            };
            return resp;
        }

        public List<Language> getExistProjectLanguages(Guid ProjectId)
        {
            var db = Resolve<DbContext>();
            var p = db.ProjectLanguages.Where(x => x.ProjectId == ProjectId).ToList();
            var count = db.ProjectKeywords.Where(x => x.ProjectId == ProjectId).Count();
            var count2 = db.ProjectLocalizations.Where(x => x.ProjectId == ProjectId && !string.IsNullOrEmpty(x.Value)).GroupBy(x => x.Language.LCID).ToDictionary(k => k.Key, i => i.Count());
            var languages = p.Select(x => new Language
            {
                EnglishName = x.Language.EnglishName,
                Id = x.Language.LCID,
                NativeName = x.Language.NativeName,
                ProjectLanguageId = x.Id,
                Name = x.Language.Name,
                IconUrl = x.Language.IconUrl,
                TotalKeywordCount = count,
                TotalKeywordTranslatedCount = count2.ContainsKey(x.Language.LCID) ? count2[x.Language.LCID] : 0,
            }).ToList();
            return languages;
        }
        [HttpPost("addprojectlanguage")]
        public AddProjectLanguageResponse AddProjectLanguage([FromBody]AddProjectLanguageRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<AddProjectLanguageResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectId))
            {
                return ResponseBase.Fail<AddProjectLanguageResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(request.ProjectId, out Guid objGuid))
            {
                return ResponseBase.Fail<AddProjectLanguageResponse>("Invalid ProjectId");
            }
            if (request.LCID == null)
            {
                return ResponseBase.Fail<AddProjectLanguageResponse>("Invalid LCID");
            }
            var db = Resolve<DbContext>();
            var resp = new AddProjectLanguageResponse();

            List<LanguageEntity> legs = new List<LanguageEntity>();
            foreach (var item in request.LCID)
            {
                var tt = db.Languages.FirstOrDefault(x => x.LCID == item);
                legs.Add(tt);

            }
            List<ProjectLanguageEntity> allLanguages = new List<ProjectLanguageEntity>();
            foreach (var item in legs)
            {
                ProjectLanguageEntity lns = new ProjectLanguageEntity
                {
                    LCID = item.LCID,
                    ProjectId = objGuid

                };
                allLanguages.Add(lns);
            }
            db.ProjectLanguages.AddRange(allLanguages);
            db.SaveChanges();
            var languages = getExistProjectLanguages(objGuid);
            resp.Langs = languages;
            //resp.Id = np.Id;
            //resp.ProjectId = np.ProjectId;
            //resp.LCID = np.LCID;
            return resp;

        }
        [HttpPost("listlanguage")]
        public GetProjectLanguageListResponse GetLanguageList([FromBody]GetProjectLanguageListRequest req)
        {
            if (req == null)
            {
                return ResponseBase.Fail<GetProjectLanguageListResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(req.ProjectKey))
            {
                return ResponseBase.Fail<GetProjectLanguageListResponse>("Invalid ProjectKey");
            }
            //if (String.IsNullOrWhiteSpace(req.ProjectId))
            //{
            //    return ResponseBase.Fail<GetProjectLanguageListResponse>("Invalid ProjectId");
            //}
            //if (!Guid.TryParse(req.ProjectId, out Guid objGuid))
            //{
            //    return ResponseBase.Fail<GetProjectLanguageListResponse>("Invalid ProjectId");
            //}
            var db = Resolve<DbContext>();
            var objGuid = db.Projects.FirstOrDefault(x => x.ProjectKey == req.ProjectKey).Id;
            var languages = getExistProjectLanguages(objGuid);
            return new GetProjectLanguageListResponse
            {
                Languages = languages
                ,

            };
        }

        [HttpPost("updateprojectlanguage")]
        public UpdateProjectLanguageResponse UpdateProjectLanguage(UpdateProjectLanguageRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<UpdateProjectLanguageResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.Id))
            {
                return ResponseBase.Fail<UpdateProjectLanguageResponse>("Invalid id");
            }
            if (!Guid.TryParse(request.Id, out Guid objGuid))
            {
                return ResponseBase.Fail<UpdateProjectLanguageResponse>("Invalid id");
            }
            if (request.LCID == null)
            {
                return ResponseBase.Fail<UpdateProjectLanguageResponse>("Invalid LCID");
            }
            var db = Resolve<DbContext>();
            var resp = new UpdateProjectLanguageResponse();
            var projectLanguage = db.ProjectLanguages.Where(x => x.Id == objGuid).FirstOrDefault();
            projectLanguage.LCID = request.LCID;
            db.SaveChanges();
            resp.Id = projectLanguage.Id;
            resp.ProjectId = projectLanguage.ProjectId;
            resp.LCID = projectLanguage.LCID;
            return resp;
        }
        [HttpPost("deleteprojectlanguage")]
        public DeleteProjectLanguageResponse DeleteProjectLanguage([FromBody]DeleteProjectLanguageRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<DeleteProjectLanguageResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectId))
            {
                return ResponseBase.Fail<DeleteProjectLanguageResponse>("Invalid id");
            }
            if (!Guid.TryParse(request.ProjectId, out Guid objGuid))
            {
                return ResponseBase.Fail<DeleteProjectLanguageResponse>("Invalid id");
            }
            if (!Guid.TryParse(request.Id, out Guid objsGuid))
            {
                return ResponseBase.Fail<DeleteProjectLanguageResponse>("Invalid id");
            }
            if (request.LCID == null)
            {
                return ResponseBase.Fail<DeleteProjectLanguageResponse>("Invalid LCID");
            }
            var db = Resolve<DbContext>();
            db.ProjectLocalizations.RemoveRange(db.ProjectLocalizations.Where(x => x.LanguageId == objsGuid));
            db.SaveChanges();
            var resp = new DeleteProjectLanguageResponse();
            var np = new ProjectLanguageEntity()
            {
                Id = objsGuid,
                ProjectId = objGuid,
                LCID = request.LCID
            };
            db.ProjectLanguages.Attach(np);
            db.ProjectLanguages.Remove(np);
            db.SaveChanges();
            resp.Id = objsGuid;
            return resp;
        }
        /******************************************************/
        /*******PROJECTLOCALIZATION REQUEST AND RESPONSE*******/
        /******************************************************/

        [HttpPost("addprojectlocalization")]
        public AddProjectLocalizationResponse AddProjectLanguage([FromBody]AddProjectLocalizationRequest req)
        {
            if (req == null)
            {
                return ResponseBase.Fail<AddProjectLocalizationResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(req.ProjectId))
            {
                return ResponseBase.Fail<AddProjectLocalizationResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(req.ProjectId, out Guid ProjIdGuid))
            {
                return ResponseBase.Fail<AddProjectLocalizationResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(req.KeywordId, out Guid KeywordIdGuid))
            {
                return ResponseBase.Fail<AddProjectLocalizationResponse>("Invalid KeywordId");
            }
            if (!Guid.TryParse(req.LanguageId, out Guid LangIdGuid))
            {
                return ResponseBase.Fail<AddProjectLocalizationResponse>("Invalid LanguageId");
            }

            var db = Resolve<DbContext>();
            var resp = new AddProjectLocalizationResponse();
            var np = new ProjectLocalizationEntity()
            {
                ProjectId = ProjIdGuid,
                KeywordId = KeywordIdGuid,
                Value = req.LocalizationValue,
                LanguageId = LangIdGuid
            };

            db.ProjectLocalizations.Add(np);
            db.SaveChanges();
            resp.Id = np.Id;
            resp.ProjectId = np.ProjectId;
            resp.KeywordId = np.KeywordId;
            resp.LocalizationValue = np.Value;
            resp.LanguageId = np.LanguageId;
            return resp;
        }

        [HttpPost("projectlocalizationvalue")]
        public GetProjectLocalizationValueResponse GetKeywordValue([FromBody]GetProjectLocalizationValueRequest request)
        {
            if (request == null)
            {
                return ResponseBase.Fail<GetProjectLocalizationValueResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(request.ProjectId))
            {
                return ResponseBase.Fail<GetProjectLocalizationValueResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(request.ProjectId, out Guid ProjIdGuid))
            {
                return ResponseBase.Fail<GetProjectLocalizationValueResponse>("Invalid ProjectId");
            }
            if (!Guid.TryParse(request.KeywordId, out Guid KeywordIdGuid))
            {
                return ResponseBase.Fail<GetProjectLocalizationValueResponse>("Invalid KeywordId");
            }
            if (!Guid.TryParse(request.LanguageId, out Guid LangIdGuid))
            {
                return ResponseBase.Fail<GetProjectLocalizationValueResponse>("Invalid LanguageId");
            };


            var db = Resolve<DbContext>();
            var resp = new GetProjectLocalizationValueResponse();
            var p = db.ProjectLocalizations.FirstOrDefault(x => x.ProjectId == ProjIdGuid && x.KeywordId == KeywordIdGuid && x.LanguageId == LangIdGuid);
            if (p != null)
            {
                resp.LocalizationValue = p.Value;
                resp.Id = p.Id;
                resp.ProjectId = p.ProjectId;
                resp.LanguageId = p.LanguageId;
                resp.KeywordId = p.KeywordId;
                return resp;
            }

            var keyw = db.ProjectKeywords.FirstOrDefault(x => x.ProjectId == ProjIdGuid && x.Id == KeywordIdGuid);
            if (keyw == null)
            {
                return ResponseBase.Fail<GetProjectLocalizationValueResponse>("Invalid Keyword id");
            }

            resp.ProjectId = ProjIdGuid;
            resp.LanguageId = LangIdGuid;
            resp.KeywordId = KeywordIdGuid;
            return resp;
        }
        [HttpPost("updateprojectlocalization")]
        public UpdateProjectLocalizationResponse UpdateProjectLanguage([FromBody]UpdateProjectLocalizationRequest req)
        {
            if (req == null)
            {
                return ResponseBase.Fail<UpdateProjectLocalizationResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(req.Id))
            {
                return ResponseBase.Fail<UpdateProjectLocalizationResponse>("Invalid Localization id");
            }
            if (!Guid.TryParse(req.Id, out Guid objGuid))
            {
                return ResponseBase.Fail<UpdateProjectLocalizationResponse>("Invalid Localization id");
            }
            if (String.IsNullOrWhiteSpace(req.Value))
            {
                return ResponseBase.Fail<UpdateProjectLocalizationResponse>("Invalid New value");
            }

            var db = Resolve<DbContext>();
            var resp = new UpdateProjectLocalizationResponse();

            var projectLocalization = db.ProjectLocalizations.Where(x => x.Id == objGuid).FirstOrDefault();
            projectLocalization.Value = req.Value;
            db.SaveChanges();
            resp.Id = projectLocalization.Id;
            resp.ProjectId = projectLocalization.ProjectId;
            resp.KeywordId = projectLocalization.KeywordId;
            resp.LocalizationValue = projectLocalization.Value;
            resp.LanguageId = projectLocalization.LanguageId;
            return resp;
        }
        [HttpPost("deleteprojectlocalization")]
        public DeletProjectLocalizationResponse DeleteProjectLanguage([FromBody]DeleteProjectLocalizationRequest req)
        {
            if (req == null)
            {
                return ResponseBase.Fail<DeletProjectLocalizationResponse>("Request Error");
            }
            if (String.IsNullOrWhiteSpace(req.Id))
            {
                return ResponseBase.Fail<DeletProjectLocalizationResponse>("Invalid Localization id");
            }
            if (!Guid.TryParse(req.Id, out Guid objGuid))
            {
                return ResponseBase.Fail<DeletProjectLocalizationResponse>("Invalid Localization id");
            }

            var db = Resolve<DbContext>();
            var resp = new DeletProjectLocalizationResponse();
            var np = new ProjectLocalizationEntity()
            {
                Id = objGuid
            };
            db.ProjectLocalizations.Attach(np);
            db.ProjectLocalizations.Remove(np);
            db.SaveChanges();
            resp.Id = objGuid;
            return resp;

        }


        /*******************************/


        [HttpPost("importkeywords")]
        public ResponseBase ImportProjectKeywords([FromForm]ImportKeywordsModel req)
        {
            if (req == null)
            {
                return new ResponseBase { Error = "Request error" };
            }
            if (req.json.Files[0].ContentType != "application/json" && req.json.Files[0].ContentType != "text/plain")
            {
                return new ResponseBase { Error = "Incorrect content type" };
            }
            if (String.IsNullOrWhiteSpace(req.ProjectId))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            if (!Guid.TryParse(req.ProjectId, out Guid objGuid))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }

            var db = Resolve<DbContext>();

            if (req.json.Files[0].ContentType=="text/plain")
            {
                String line;
                List<string> arrayOfAllKeys = new List<string>();
                try
                {
                    var fileStream = req.json.Files[0].OpenReadStream();
                    StreamReader sr = new StreamReader(fileStream);
                    line = sr.ReadLine();
                    
                    while (line != null)
                    {
                        
                        arrayOfAllKeys.Add(line);
                        line = sr.ReadLine();
                    }
                    foreach (var item in arrayOfAllKeys)
                    {
                        var entity = db.ProjectKeywords.FirstOrDefault(x => x.Keyword == item && x.ProjectId == objGuid);
                        if (entity == null)
                        {
                            ProjectKeywordEntity newEntity = new ProjectKeywordEntity()
                            {
                                Keyword = item,
                                ProjectId = objGuid
                            };
                            db.ProjectKeywords.Add(newEntity);

                        }

                    }
                    sr.Close();

                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                }

            }

            if (req.json.Files[0].ContentType == "application/json")
            {
                var fileStream = req.json.Files[0].OpenReadStream();
                List<string> arrayOfAllKeys = new List<string>();
                using (var reader = new StreamReader(fileStream))
                {
                    var content = reader.ReadToEnd();
                    var json = JObject.Parse(content);

                    //Dictionary<string, string> dictObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(json.ToString());
                    //arrayOfAllKeys = dictObj.Keys.ToList();
                    dynamic myObj = JsonConvert.DeserializeObject(json.ToString());
                    string key = "";
                    var list = new Dictionary<string, string>();
                    PrintObject(myObj, key, list);
                    arrayOfAllKeys = list.Keys.ToList();
                }

                foreach (var item in arrayOfAllKeys)
                {
                    var entity = db.ProjectKeywords.FirstOrDefault(x => x.Keyword == item && x.ProjectId == objGuid);
                    if (entity == null)
                    {
                        ProjectKeywordEntity newEntity = new ProjectKeywordEntity()
                        {
                            Keyword = item,
                            ProjectId = objGuid
                        };
                        db.ProjectKeywords.Add(newEntity);
                    }
                }
                
            }


            db.SaveChanges();
            JObject result = new JObject()
            {

            };
            //
            return new ResponseBase();
        }

        private static void PrintObject(JToken token, string key, Dictionary<string, string> list)
        {
            if (token is JProperty)
            {
                var jProp = (JProperty)token;
                var val = jProp.Value is JValue ? ((JValue)jProp.Value).Value : "";



                foreach (var child in jProp.Children())
                {

                    if (!child.HasValues)
                    {
                        key += jProp.Name;
                    }
                    else
                    {
                        key += jProp.Name + ":::";
                    }
                    PrintObject(child, key, list);

                }
                if (val != "")
                {
                    //Console.WriteLine($"{key} : {val}");

                    list.Add(key, val.ToString());

                }
            }
            else if (token is JObject)
            {
                foreach (var child in ((JObject)token).Children())
                {
                    PrintObject(child, key, list);
                }
            }
        }

        [HttpPost("importkeydslocales")]
        public ResponseBase ImportProjectKeywordsLocalization([FromForm]ImportKeywordLocalizationModel req)
        {
            if (req == null)
            {
                return new ResponseBase { Error = "Request error" };
            }
            if (req.json.Files[0].ContentType != "application/json")
            {
                return new ResponseBase { Error = "Incorrect content type" };
            }
            if (String.IsNullOrWhiteSpace(req.ProjectId))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            if (!Guid.TryParse(req.ProjectId, out Guid ProjIdGuid))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            if (String.IsNullOrWhiteSpace(req.LangId))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            if (!Guid.TryParse(req.LangId, out Guid languageId))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            var fileStream = req.json.Files[0].OpenReadStream();
            List<string> arrayOfAllKeys = new List<string>();
            List<string> arrayOfAllValues = new List<string>();
            Dictionary<string, string> dictObj;
            var list = new Dictionary<string, string>();
            using (var reader = new StreamReader(fileStream))
            {
                var content = reader.ReadToEnd();
                var json = JObject.Parse(content);
                dynamic myObj = JsonConvert.DeserializeObject(json.ToString());
                string key = "";

                PrintObject(myObj, key, list);
                //dictObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(json.ToString());
                arrayOfAllKeys = list.Keys.ToList();
                arrayOfAllValues = list.Values.ToList();
            }
            var db = Resolve<DbContext>();

            string SQL = @"DECLARE @TEMPPROJTABLE TABLE (
                Id uniqueidentifier PRIMARY KEY default NEWID() NOT NULL,
                ProjectId uniqueidentifier NOT NULL,
                KeywordId uniqueidentifier NULL,
                Keyword nvarchar(3000) NOT NULL,
	            [Value] nvarchar(3000) NOT NULL,
	            LanguageId uniqueidentifier NOT NULL,
	            UpdatedAt datetime2(0)
                );
 
                INSERT INTO @TEMPPROJTABLE (ProjectId, 
				Keyword,
	            [Value],
	            LanguageId)
                VALUES
                ";
            bool valuesEntered = false;
            foreach (var item in list)
            {
                if (valuesEntered)
                {
                    SQL += @",";
                }
                SQL += $@"(
                '{ProjIdGuid}',
                N'{item.Key.Replace("'", "''")}',
                N'{item.Value.Replace("'", "''")}',
                '{languageId}'
                )";
                valuesEntered = true;
            }

            SQL += @"
            Update t1 Set t1.KeywordId = pk.Id
				FROM @TEMPPROJTABLE t1
				INNER Join ProjectKeywords as pk on t1.ProjectId = pk.ProjectId And t1.Keyword = pk.Keyword

            Merge into ProjectLocalization as TARGET
                using(select * from @TEMPPROJTABLE where KeywordId is not null) as SOURCE 
                        on TARGET.ProjectId = SOURCE.ProjectId And TARGET.KeywordId = SOURCE.KeywordId And TARGET.LanguageId = SOURCE.LanguageId
                    when not matched by TARGET then 
                        insert values(SOURCE.Id,SOURCE.ProjectId,SOURCE.KeywordId,SOURCE.[VALUE],SOURCE.LanguageId,SOURCE.UpdatedAt);";

            db.Database.ExecuteSqlCommand(SQL);
            db.SaveChanges();
            return new ResponseBase();
        }

        [HttpPost("importExcel")]
        public ResponseBase ImportExcel([FromForm]ImportExcel req)
        {
            if (req == null)
            {
                return new ResponseBase { Error = "Request error" };
            }
            if (req.excel.Files[0].ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                return new ResponseBase { Error = "Incorrect content type" };
            }
            if (String.IsNullOrWhiteSpace(req.ProjectId))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            if (!Guid.TryParse(req.ProjectId, out Guid ProjIdGuid))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            if (String.IsNullOrWhiteSpace(req.LangId))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            if (!Guid.TryParse(req.LangId, out Guid languageId))
            {
                return ResponseBase.Fail<ResponseBase>("Invalid ProjectId");
            }
            var db = Resolve<DbContext>();
            string SQLgetProjectLangs = $@"SELECT pl.Id,l.EnglishName FROM ProjectLanguages as pl
                                        LEFT JOIN Languages l on l.LCID = pl.LCID
                                        WHERE ProjectId = '{ProjIdGuid}'";

            var ProjectLangs = db.Database.SqlQuery<ProjectLanguage>(SQLgetProjectLangs).ToList();


            var keywordColum = "B";
            ResponseBase resp = new ResponseBase();
            var fileStream = req.excel.Files[0].OpenReadStream();
            if (fileStream == null)
            {
                resp.Error = "File Is Null";
                return resp;
            }
            ExcelPackage excelPkg;
            try
            {
                excelPkg = new ExcelPackage(fileStream);
            }
            catch (Exception)
            {
                resp.Error = "Excel file error";
                return resp;
            }
            int HebrewColumn = 0;
            int EnglishColumn = 0;
            int RussianColumn = 0;
            foreach (var ws in excelPkg.Workbook.Worksheets)
            {


                //var index = excelPkg.Workbook.Worksheets.FirstOrDefault(x => x.Name == "Search").Index;
                //var ws = excelPkg.Workbook.Worksheets[index];
                for (int i = 1; i < 6; i++)
                {
                    var name = ws.Cells[1, i].Text;
                    if (name == "Hebrew")
                    {
                        HebrewColumn = i;
                    }
                    if (name == "English")
                    {
                        EnglishColumn = i;
                    }
                    if (name == "Russian")
                    {
                        RussianColumn = i;
                    }
                }
                string sql = "";
                var rowExist = true;
                int indexIteration = 2;
                Dictionary<string, string> resultKeywords = new Dictionary<string, string>();

                while (rowExist)
                {
                    var Key = ws.Cells[indexIteration, 2].Text;
                    var Value = ws.Cells[indexIteration, HebrewColumn].Text;
                    indexIteration++;
                    if (string.IsNullOrWhiteSpace(Key))
                    {
                        rowExist = false;
                    }
                    else
                    {
                        resultKeywords.Add(Key.Replace(".", ":::"), Value);
                    }
                }

                string SQLInserHebrewKeys = "";
                foreach (var item in resultKeywords)
                {
                    SQLInserHebrewKeys += $@"INSERT INTO ProjectKeywords (ProjectId, Keyword) VALUES ('{ProjIdGuid}', '{item.Key}');
                ";
                }

                db.Database.ExecuteSqlCommand(SQLInserHebrewKeys);
                db.SaveChanges();

                string SQLInserValues = "";
                foreach (var item in resultKeywords)
                {
                    SQLInserValues += $@"INSERT INTO ProjectLocalization (ProjectId, KeywordId, Value, LanguageId) 
                    VALUES ('{ProjIdGuid}', (Select Id From ProjectKeywords Where Keyword = '{item.Key}'),
                    N'{item.Value.Replace("'", "''")}', '{ProjectLangs.FirstOrDefault(x => x.EnglishName == "Hebrew").Id}'); ";
                }
                db.Database.ExecuteSqlCommand(SQLInserValues);
                db.SaveChanges();


                Dictionary<string, string> resultEnglish = new Dictionary<string, string>();
                indexIteration = 2;
                rowExist = true;
                while (rowExist)
                {
                    var Key = ws.Cells[indexIteration, 2].Text;
                    var Value = ws.Cells[indexIteration, EnglishColumn].Text;
                    indexIteration++;
                    if (string.IsNullOrWhiteSpace(Key))
                    {
                        rowExist = false;
                    }
                    else
                    {
                        resultEnglish.Add(Key.Replace(".", ":::"), Value);
                    }
                }

                string SQLInserEnglishValues = "";
                foreach (var item in resultEnglish)
                {
                    SQLInserEnglishValues += $@"INSERT INTO ProjectLocalization (ProjectId, KeywordId, Value, LanguageId) 
                    VALUES ('{ProjIdGuid}', (Select Id From ProjectKeywords Where Keyword = '{item.Key}'),
                    N'{item.Value.Replace("'", "''")}', '{ProjectLangs.FirstOrDefault(x => x.EnglishName == "English").Id}'); ";
                }
                db.Database.ExecuteSqlCommand(SQLInserEnglishValues);
                db.SaveChanges();
                //
                Dictionary<string, string> resultRussian = new Dictionary<string, string>();
                indexIteration = 2;
                rowExist = true;
                while (rowExist)
                {
                    var Key = ws.Cells[indexIteration, 2].Text;
                    var Value = ws.Cells[indexIteration, RussianColumn].Text;
                    indexIteration++;
                    if (string.IsNullOrWhiteSpace(Key))
                    {
                        rowExist = false;
                    }
                    else
                    {
                        resultRussian.Add(Key.Replace(".", ":::"), Value);
                    }
                }

                string SQLInserRussianValues = "";
                foreach (var item in resultRussian)
                {
                    SQLInserRussianValues += $@"INSERT INTO ProjectLocalization (ProjectId, KeywordId, Value, LanguageId) 
                    VALUES ('{ProjIdGuid}', (Select Id From ProjectKeywords Where Keyword = '{item.Key}'),
                    N'{item.Value.Replace("'", "''")}', '{ProjectLangs.FirstOrDefault(x => x.EnglishName == "Russian").Id}'); ";
                }
                db.Database.ExecuteSqlCommand(SQLInserRussianValues);

                db.SaveChanges();
            }
            return resp;
        }

    }
}

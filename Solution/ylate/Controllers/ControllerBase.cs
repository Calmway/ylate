﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ylate.Controllers
{
    public class YlateMvcControllerBase : Microsoft.AspNetCore.Mvc.Controller
    {
        protected readonly ILifetimeScope LifetimeScope;

        public YlateMvcControllerBase(ILifetimeScope lifetimeScope)
        {
            LifetimeScope = lifetimeScope;
        }

        protected T Resolve<T>()
        {
            var result = LifetimeScope.Resolve<T>();
            return result;
        }

        protected bool TryResolve<T>(out T instance)
        {
            var result = LifetimeScope.TryResolve(out instance);
            return result;
        }

        protected T ResolveNamed<T>(string serviceName)
        {
            var result = LifetimeScope.ResolveNamed<T>(serviceName);
            return result;
        }

        protected T ResolveKeyed<T>(object key)
        {
            var result = LifetimeScope.ResolveKeyed<T>(key);
            return result;
        }

    }
}

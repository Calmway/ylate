﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ylate.EF;
using ylate.Models;
using System.Resources;
using Newtonsoft.Json.Linq;

namespace ylate.Controllers
{
    [Route("api")]
    public class ApiController : YlateMvcControllerBase
    {
        public string FromDictionaryToJson(Dictionary<string, string> dictionary)
        {
            var perem = JsonConvert.SerializeObject(dictionary.Select(x => new KeyValuePair<string, string>(x.Key.Replace(":::", "."), x.Value)).ToDictionary(x=>x.Key,x=>x.Value));
            return perem;
        }
        public class QueryResult
        {
            public string A { get; set; }
            public string B { get; set; }
        }
        void Nested(JObject root, KeyValuePair<string, string> kv, string separator)
        {
            string[] NESTED_SEPARATOR = new[] { separator };
            var parts = kv.Key.Split(NESTED_SEPARATOR, StringSplitOptions.RemoveEmptyEntries);
            var last = parts.Aggregate(root, reducer);
            ((JProperty)last.Parent).Value = kv.Value;

            // local funcs
            JObject reducer(JObject currentJobj, string properyName)
            {
                if (currentJobj.TryGetValue(properyName, StringComparison.InvariantCulture, out JToken jtoken))
                {
                    return (jtoken as JObject) ?? new JObject();
                }

                var newJobj = new JObject();
                currentJobj.Add(properyName, newJobj);
                return newJobj;
            }
        }

        public ApiController(ILifetimeScope lifetimeScope) : base(lifetimeScope) { }
        [HttpGet("export")]
        public async Task<IActionResult> Get(ExportRequestModel req)
        {
            if (req == null)
            {
                return BadRequest("Bad Request");
            }
            if (String.IsNullOrWhiteSpace(req.ProjectKey))
            {
                return BadRequest("Project id is empty");
            }
            if (req.LCID <= 0)
            {
                return BadRequest("Incorrect language id");
            }
            if (String.IsNullOrWhiteSpace(req.Format))
            {
                return BadRequest("Format is empty");
            }

            string LangName = "";
            int langId;
            Dictionary<string, string> KeyValue = new Dictionary<string, string>();

            using (var db = Resolve<DbContext>())
            {
                var projectlanguage = db.ProjectLanguages.FirstOrDefault(x => x.Project.ProjectKey == req.ProjectKey && x.Language.LCID == req.LCID);
                if (projectlanguage == null)
                {
                    return BadRequest("Not found.");
                }

                string SQL = $@"SELECT
	                Keyword as [A],[Value] as [B]
                FROM
	                ProjectKeywords
	                LEFT JOIN ProjectLocalization ON ProjectLocalization.KeywordId=ProjectKeywords.Id
                WHERE
	                ProjectLocalization.ProjectId = '{projectlanguage.Project.Id}' AND ProjectLocalization.LanguageId='{projectlanguage.Id}';";


                KeyValue = db.Database.SqlQuery<QueryResult>(SQL).ToDictionary(r => r.A, r => r.B);
                langId = projectlanguage.LCID;
                LangName = projectlanguage.Language.Name;
            }

            return GenerateFile(KeyValue, LangName, req.Format);
        }

        [HttpPost("download")]
        public async Task<IActionResult> Upload([FromBody]DownloadRequestModel req)
        {
            if (req == null)
            {
                return BadRequest("Bad Request");
            }
            if (String.IsNullOrWhiteSpace(req.ProjectId))
            {
                return BadRequest("Project id is empty");
            }
            if (!Guid.TryParse(req.ProjectId, out Guid ProjGuid))
            {
                return BadRequest("Incorrect project id");
            }
            if (String.IsNullOrWhiteSpace(req.LanguageId))
            {
                return BadRequest("Incorrect language id");
            }
            if (!Guid.TryParse(req.LanguageId, out Guid LangGuid))
            {
                return BadRequest("Incorrect project id");
            }
            if (String.IsNullOrWhiteSpace(req.Format))
            {
                return BadRequest("Format is empty");
            }
            string SQL = $@"SELECT
	            Keyword as [A],[Value] as [B]
            FROM
	            ProjectKeywords
	            LEFT JOIN ProjectLocalization ON ProjectLocalization.KeywordId=ProjectKeywords.Id
            WHERE
	            ProjectLocalization.ProjectId = '{ProjGuid}' AND ProjectLocalization.LanguageId='{LangGuid}'";

            string LangName = "";
            int langId;
            Dictionary<string, string> KeyValue = new Dictionary<string, string>();
            using (var db = Resolve<DbContext>())
            {
                KeyValue = db.Database.SqlQuery<QueryResult>(SQL).ToDictionary(r => r.A, r => r.B);
                langId = db.ProjectLanguages.FirstOrDefault(y => y.ProjectId == ProjGuid && y.Id == LangGuid).LCID;
                LangName = db.Languages.FirstOrDefault(x => x.LCID == langId).Name;
            }
            return GenerateFile(KeyValue, LangName, req.Format);

        }
        private IActionResult GenerateFile(Dictionary<string, string> KeyValue, string LangName, string Format)
        {
            if (Format == "flat")
            {
                var json = FromDictionaryToJson(KeyValue);
                byte[] bytes = Encoding.UTF8.GetBytes(json);

                return File(
                    fileContents: bytes,
                    contentType: "application/json; charset=utf-8",
                    fileDownloadName: $"{LangName}.json"
                );
            }
            else if (Format == "resx")
            {
                using (MemoryStream gigaByte = new MemoryStream())
                using (ResXResourceWriter resx = new ResXResourceWriter(gigaByte))
                {
                    foreach (var item in KeyValue)
                    {
                        resx.AddResource(new ResXDataNode(item.Key.Replace(":::", "."), item.Value));
                    }
                    resx.Close();
                    byte[] bytes = gigaByte.ToArray();
                    return File(
                        fileContents: bytes,
                        contentType: "application/xml; charset=utf-8",
                        fileDownloadName: $"{LangName}.resx"
                    );
                }
            }
            else if (Format == "nested")
            {
                //if (String.IsNullOrWhiteSpace(SplitValue))
                //{
                //    return BadRequest("Incorrect Split Value");
                //}
                JObject result = new JObject();

                foreach (var kv in KeyValue)
                {
                    Nested(result, kv, ":::");
                }

                byte[] bytes = Encoding.UTF8.GetBytes(result.ToString(Formatting.None));

                return File(
                    fileContents: bytes,
                    contentType: "application/json; charset=utf-8",
                    fileDownloadName: $"{LangName}.json"
                );

            }
            else
            {
                return BadRequest("Incorrect format");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ylate.EF
{
    [Table("Projects")]
    public class ProjectEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string ProjectName { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity), DataMember]
        public DateTime CreatedAt { get; set; }
        public string ProjectKey { get; set; }
        public virtual List<ProjectKeywordEntity> Keywords { get; set; }

        public virtual List<ProjectLanguageEntity> Languages { get; set; }
    }
}

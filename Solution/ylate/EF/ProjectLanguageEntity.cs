﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ylate.EF
{
    [Table("ProjectLanguages")]
    public class ProjectLanguageEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public virtual ProjectEntity Project { get; set; }

        public int LCID { get; set; }

        [ForeignKey(nameof(LCID))]
        public virtual LanguageEntity Language { get; set; }
    }
}

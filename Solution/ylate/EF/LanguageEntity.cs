﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ylate.EF
{
    [Table("Languages")]
    public class LanguageEntity
    {
        [Key]
        public int LCID { get; set; }

        public string Name { get; set; }

        public string EnglishName { get; set; }

        public string NativeName { get; set; }
        public string IconUrl { get; set; }
    }
}

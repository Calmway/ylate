﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ylate.EF
{
    [Table("ProjectLocalization")]
    public class ProjectLocalizationEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        [ForeignKey(nameof(ProjectId))]
        public ProjectEntity Project { get; set; }

        public Guid KeywordId { get; set; }

        [ForeignKey(nameof(KeywordId))]
        public ProjectKeywordEntity Keyword { get; set; }

        public Guid LanguageId { get; set; }

        [ForeignKey(nameof(LanguageId))]
        public ProjectLanguageEntity Language { get; set; }


        public string Value { get; set; }
    }
}

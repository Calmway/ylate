﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ylate.EF
{
    public class DbContext : System.Data.Entity.DbContext
    {
        public DbContext(string connString) : base(connString)
        {
            Database.SetInitializer<DbContext>(null);
        }

        public DbSet<LanguageEntity> Languages { get; set; }

        public DbSet<ProjectEntity> Projects { get; set; }

        public DbSet<ProjectKeywordEntity> ProjectKeywords { get; set; }

        public DbSet<ProjectLanguageEntity> ProjectLanguages { get; set; }

        public DbSet<ProjectLocalizationEntity> ProjectLocalizations { get; set; }
    }
}

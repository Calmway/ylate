import Vue from "vue";
import Router from "vue-router";
import { Projects,ProjectKeywords, ProjectLanguage,ProjectKeywordsAndLocalization } from "./views";

Vue.use(Router);

const router = new Router({
  routes: [
    // {
    //   meta:{title:"Login", fullscreen: true, AllowAnonymous: true },
    //   path: "/Login",
    //   name: "Login",
    //   component: Login
    // },
    {
      meta:{title:"Projects"},
      path: "/",
      name: "Projects",
      component: Projects
    },
    {
      meta:{title:"ProjectLanguage"},
      path: '/ProjectLanguage/:projectName',
      name: 'ProjectLanguage',
      component: ProjectLanguage,
    },
    {
      meta:{title:"ProjectKeywords"},
      path: '/ProjectKeywords/:projectName',
      name: "ProjectKeywords",
      component: ProjectKeywords
    },
    {
      meta:{title:"ProjectKeywordsAndLocalization"},
      path: '/ProjectKeywordsAndLocalization/:projectName/:lcid',
      name: 'ProjectKeywordsAndLocalization',
      component: ProjectKeywordsAndLocalization
    },
  ]
});

// router.beforeEach(async function (to, from, next) {
//   if (to.path === '/') {
//     next({ 'path': '/Login' })
//     return
//   }

//   /**
//    * Auth guard
//    */
//   if (!to.meta.AllowAnonymous && !Auth.IsAuthenticated) {
//     next({ 'path': '/Login' })
//     return
//   }

//   /**
//    * Already voted guard
//    */
//   // var skipNames = ['Login', 'ThankYou']
//   // if (skipNames.indexOf(to.name) === -1) {
//   //   var checkVoteResult = await api.CheckAlreadyVoted()
//   //   if (checkVoteResult.error) {
//   //     helper.swalWarning({html: checkVoteResult.error})
//   //     return
//   //   }
//   // }

//   next()
// });
export default router;
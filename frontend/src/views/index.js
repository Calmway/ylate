import Projects from "./Projects.vue";
import ProjectLanguage from "./ProjectLanguage.vue";
import ProjectKeywords from "./ProjectKeywords.vue";
import ProjectKeywordsAndLocalization from "./ProjectKeywordsAndLocalization.vue";


export { Projects,ProjectLanguage,ProjectKeywords,ProjectKeywordsAndLocalization };

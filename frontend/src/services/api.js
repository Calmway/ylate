import axiosLib from "axios";
import to from "await-to-js";

var axios = axiosLib.create({

});

var api = {
  async axiosWrapper(req) {
    let err, axiosResponse;
    var promise = axios(req);
    var result = await to(promise);
    err = result[0];
    axiosResponse = result[1];

    return [err, axiosResponse];
  },
  async getAllProjectList(){
    var url = "api/project/list";
    var reqData = {};
    var req = { method: "get", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async createNewProject(projectName,projectnameKey){
    var url = "api/project/create";
    var reqData = {NewName : projectName, ProjectKey: projectnameKey};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async updateProjectName(newname, id) {
    var url = "api/project/update";
    var reqData = {NewName: newname, Id: id};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async deleteProject(id){
    var url = "api/project/delete";
    var reqData = {Id: id};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async getLanguageList() {
    var url = "api/project/listprojectlanguage";
    var req = { method: "get", url: url };
    return await this.axiosWrapper(req);
  },
  async addProjectLanguage(lcid,projid) {
    var url = "api/project/addprojectlanguage";
    var reqData = {LCID:lcid,ProjectId: projid};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async getProjectLanguages(projectnamekey){
    var url = "api/project/listlanguage";
    var reqData = {ProjectKey: projectnamekey};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async deleteProjectLanguage(projid,lcid,id) {
    var url = "api/project/deleteprojectlanguage";
    var reqData = {ProjectId: projid,LCID:lcid,Id:id};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async getProjectKeywords(projectkey,lcid) {
    var url = "api/project/projectlistkeyword";
    var reqData = {ProjectKey: projectkey,LCID:lcid};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  
  async getOnlyProjectKeywords(projectkey) {
    var url = "api/project/projectkeywords";
    var reqData = {ProjectKey: projectkey};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async getSearchedProjectKeywords(projectkey, searchingKeyword) {
    var url = "api/project/searchprojectkeywords";
    var reqData = {ProjectKey: projectkey, ProjectKeyword: searchingKeyword};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async addnewKeywordToProject(newKeyword,projectid) {
    var url = "api/project/addkeyword";
    var reqData = {NewKeyword: newKeyword,ProjectId: projectid};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async editProjectKeyword(newKeyword,id) {
    var url = "api/project/updatekeyword";
    var reqData = {NewKeyword: newKeyword,Id: id};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async deleteProjectKeyword(id) {
    var url = "api/project/deletekeyword";
    var reqData = {Id: id};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async deleteProjectKeywords(projectId) {
    var url = "api/project/deletekeywords";
    var reqData = {ProjectId: projectId};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async addLocalization(projectid,keywordid,localval,langid) {
    var url = "api/project/addprojectlocalization";
    var reqData = {ProjectId: projectid,KeywordId: keywordid,LocalizationValue:localval,LanguageId:langid};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async getProjectLanguage(projectId,lcid){
    var url = "api/project/projectlanguageId";
    var reqData = {ProjectId: projectId, LCID:lcid};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async getLocalization(projectId,keywordId,langId){
    var url = "api/project/projectlocalizationvalue";
    var reqData = {ProjectId: projectId,KeywordId: keywordId,LanguageId:langId};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async updLocalization(id,newlocal){
    var url = "api/project/updateprojectlocalization";
    var reqData = {Id: id,Value:newlocal};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async delLocalization(id){
    var url = "api/project/deleteprojectlocalization";
    var reqData = {Id: id};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async ImportJsonKeywords(json){
    var url = "api/project/importkeywords";
    var req = { method: "post", url: url, data: json, headers: {"Content-Type": 'multipart/form-data'}};
    return await this.axiosWrapper(req);
  },
  async ImportJsonKeywordsLocales(json){
    var url = "api/project/importkeydslocales";
    var req = { method: "post", url: url, data: json, headers: {"Content-Type": 'multipart/form-data'}};
    return await this.axiosWrapper(req);
  },
  async ImportJsonKeywordsLocalesExcel(excel){
    var url = "api/project/importExcel";
    var req = { method: "post", url: url, data: excel, headers: {"Content-Type": 'multipart/form-data'}};
    return await this.axiosWrapper(req);
  },
  async getProjectNameInLocalization(projectnamekey){
    var url = "api/project/getprojid";
    var reqData = {ProjectKey: projectnamekey};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async getProjectLangName(lcid){
    var url = "api/project/projectlangname";
    var reqData = {LCID: lcid};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  },
  async ExportFile(projectid,langid,format) {
    var url = "api/download";
    var reqData = {ProjectId: projectid,LanguageId:langid,Format:format};
    var req = { method: "post", url: url, data: reqData };
    return await this.axiosWrapper(req);
  }
};

export default api;

export default {
    filterId: null,
    filterName: null,
    filterkeyId:null,
    filterlangId:null,
    filterLangName: null,

    setFilterElement(ElementId,ElementName) {
        this.filterId = ElementId;
        this.filterName = ElementName;
    },
    setFilterKeyElement(ElementId,langId,filterlagname) {
        this.filterkeyId = ElementId;
        this.filterlangId = langId;
        this.filterLangName = filterlagname;
    },
    getFilterElement() {
        if (this.filterId == null) {
            return null
        }
        else return { id: this.filterId, text: this.filterName };
    },

    clearFilterElement() {
        this.filterId = null,
        this.filterName = null
    }

}
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/fontawesome-all.css';
import './assets/css/animate.css';
import './assets/css/style.css';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import $ from 'jquery';
Vue.prototype.$ = $;
import "popper.js";

import 'metismenu';
import './assets/js/inspinia.js';
import './assets/js/jquery.slimscroll.js';
import 'bootstrap';



Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: router,
}).$mount('#app')
